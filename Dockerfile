FROM openjdk:11.0.9-jdk-slim
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
COPY ./target/*.jar /opt
ARG BRANCH
ENV BRANCH ${BRANCH}
CMD java -jar -Xms12M -Xmx32M -Dspring.profiles.active=${BRANCH} /opt/demo-0.0.1-SNAPSHOT.jar
EXPOSE 8080