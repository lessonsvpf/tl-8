package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @Value("${MainController.env}")
    private String env;
	@GetMapping("/")
	public String home(Model model) {
		model.addAttribute("title", env);
		return "home";
	}

}